#!/usr/bin/python3
# Carmen González Lopez
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import sys
import socketserver
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Echo server class"""

    dicc = {}

    def register2json(self):
        """crea y escribe en el fichero json"""
        with open('registered.json', 'w') as file:
            json.dump(self.dicc, file, indent=4)

    def json2register(self):
        """si existe el fichero, lo lee su y lo modifica si es necesario"""
        try:
            with open('registered.json', 'r') as archivo_file:
                self.dicc = json.load(archivo_file)
                print(self.dicc)
        except:
            pass

    def handle(self):
        """ handle method of the server class"""
        self.json2register()
        line = self.rfile.read()
        peticion = line.decode('utf-8').split()
        solicitud = peticion[1].split(':')
        user = solicitud[1]
        expire = int(peticion[4])
        """tiempo actual mas expiracion"""
        time_now = time.time() + expire
        time_ahora = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time_now))

        if peticion[0] == 'REGISTER':
            datos_user = {'ip': self.client_address[0], 'expires': time_ahora}
            self.dicc[user] = datos_user
            print('Nuestros usuarios:')
            print('diccionario', self.dicc)

        if peticion[3] == 'Expires:':
            if expire == 0:
                del self.dicc[user]
                print('El usuario ha sido eliminado.')
                print('diccionario', self.dicc)
            else:
                print('El usuario no ha cambiado.')

        self.wfile.write(b' SIP/2.0 200 OK\r\n\r\n')
        self.register2json()

if __name__ == "__main__":
    """ comentario"""
    serv = socketserver.UDPServer(('', int(sys.argv[1])), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
