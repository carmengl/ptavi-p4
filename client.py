#!/usr/bin/python3
# Carmen Gonzalez Lopez
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
try:
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    REGISTER = sys.argv[3]
    USER = sys.argv[4]
    EXPIRES = sys.argv[5]
except (IndexError, ValueError):
    sys.exit('Usage: client.py ip puerto register sip_address expires_value')

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    peticion_sip = ("REGISTER sip:" + USER + ' SIP/2.0\r\n' +
                    "Expires: " + EXPIRES + '\r\n\r\n')
    print(peticion_sip)
    my_socket.send(bytes(peticion_sip, 'utf-8'))
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
